      ____  ____  _____ ____                            
     / __ )/ __ \/ ___// __ \                           
    / __  / / / /\__ \/ /_/ /                                )   )  ()
   / /_/ / /_/ /___/ / ____/                                 )   )   \
  /_____/\____//____/_/                                    _____________
         ___   ____         ___                            \           / 
   _  __/ _ \ / __/  ____  / _ | ___  ___ ___ _____         \  o   o  /
  | |/ / // // _ \  /___/ / __ |/ _ \/ _ `/ // (_-<          \_______/
  |___/\___(_)___/       /_/ |_/_//_/\_, /\_,_/___/          /       \
                                    /___/               

		~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Welcome to the Barbeque Open Source Project (BOSP), version 0.6 ("Angus").

The Barbeque RTRM is an open-source framework for dynamic resource management
of new generation many-core systems, and a corresponding open-source project
led by Politecnico di Milano.
The Barbeque Open Source Project is sponsorized by the 2PARMA FP7 EU founded
project.

This document will show you how to compile and run the Barbeque RTRM framework.
A set of accompaning examples are provided to better explain with simple
examples how to code an applications which want to access resources managed by
this framework.


Index
~~~~~
.:: 1 Managing the BOSP sources tree
    1.1 download the sources
    1.2 keep sources in sync

.:: 2 Compiling the BOSP
    2.1 compilation requirements
    2.2 run the compilation
    2.3 getting to know the installation process

.:: 3 Running the Barbeque RTRM
    3.1 setup the configuration file
    3.2 run the resource manager

.:: 4 Running the provided Tutorials
    4.1 using the tutorials


1. Managing the BOSP source tree
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get the Barbeque Open Source Project sources, you’ll need to get familiar
with Git and Repo. A good reference and starting point is provided by the
Android wiki:
http://www.omappedia.org/wiki/Android_Miscellaneous

.:: Installing Repo
You need a working repo binary in order to setup the BOSP repository. You could
get one by downloading it from the Android repository and installing it locally
(e.g. in a “bin” sub-folder of you home directory). Than, ensure to make it
executable:

  $ curl http://android.git.kernel.org/repo >~/bin/repo
  $ chmod a+x ~/bin/repo

.:: Creating a Working Directory
The complete BOSP sources are downloaded and compiled into a user-defined
working directory. Thus, start by creating such a folder, wherever you prefer
and enter it:

  $ cd ~
  $ mkdir working-directory-name
  $ cd working-directory-name

For the purposes of this tutorial, thereafter I will assume your working
directory is ~/BOSP

.:: Initialize repo in your working directory
To initialize your local repository using the Barbeque Open Source Project
trees, use this command from inside your working directory:

  $ repo init -u git://gitorious.org/bosp/manifest.git -b master

'-b' is the command line option for branch, and I'm assuming you want the
‘master’ branch.  This command will download the BOSP manifest file which
contains all the information to download the required source trees.
At the end, this command ask also you some information about your name and
email. These are pre-configured to allows you to submit patches to the
project.

.:: Sync up with the remote repository
Once the manifest has been initialized, you could synchronize you local BOSP
source tree with the main repository. This allows to download all the last
version of the provided source packages. To that purpose, form within you
working directory, simply run:

  $ repo sync

This synchronization downloads all the most recent version of the sources, for
each library and code related to the BOSP.
This operation should be performed each time we want to update our BOSP source
tree with the latest patches added to the main project.

2. Compiling the BOSP
~~~~~~~~~~~~~~~~~~~~~

.:: Compilation requirements

Once the synchronization has completed, you get a complete building tree of all
required component, indeed, the BOSP aims at to be a self-contained project.
The only thing you are missing is a valid compilation toolchain, it is required
g++ >= 4.5 and libstdc++-v3 >= 4.5.

Please note that (for the time being) the BOSP building system does not provide
cross compilation support: thus, it is still possible to cross-compile each
component but this must be done by hand.

.:: Run the compilation

After a repo sync you end up with a Makefile placed at the root of your working
directory. To run the compilation simply run:

  $ make

This command starts the compilation and installation, in the proper order, of
all the BOSP components. Please be patient, especially the first compilation
run requires to build a lot of external compoents! Successive runs should be
faster.

Each step of the compilation of a Barbeque RTRM related component (e.g. the
framework itself and the contributed codes), provides a detailed report on how
it is compiled and where the installation procedure will deploy the generated
files. This is for example the summary of the default Barbeque RTRM build:

  -- =====[ Barbeque RTRM - Building System Configuration ]=====================
  -- Barbeque Version...... drk_for_v0.6-rc1-108-g1f6c9d9
  -- Build type............ Release
  -- Installation prefix... /opt/BBQ_BuildingystemTest/out
  --    Barbeuqe RTRM...... <prefix>/sbin   
  --    Configuration...... <prefix>/etc/bbque
  --    Recipes............ <prefix>/etc/bbque/recipes
  --    Plugins............ <prefix>/lib/bbque/plugins
  --    Var(RPC-Channel)... <prefix>/var/bbque
  --    Headers............ <prefix>/include/bbque
  --    RTLib library...... <prefix>/lib/bbque
  --    Documentation...... <prefix>/usr/share/bbque
  --
  -- Default values could be changes at command line, with:
  --   cmake -D<Variable>=<Value>
  -- or use the '-i' cmake flag to run in interactive mode
  -- ===========================================================================

This report show:
- the build version (drk_for_v0.6-rc1-108-g1f6c9d9)
	which, for example, should be referenced in bug reports
- the build type (Release)
	which could be either Debug or Release
- the installation PREFIX
	which specify both where the build system expect to find the required
	dependencies (e.g. external libraries) and also the root of the
	installation procedure
- the specific installation path of each component
	which refers to the previously defined prefix.

All these configuration options could be customized at cmake configuration time.
However, for the time being, the provided (simple) Makefile uses a pre-defined
configuration which should be enought for the generic user (e.g. application
developer).


.:: Getting to know the installation process

The BOSP is organized in modules, right now the source tree is composed by
these folders:

  derkling@darkstar:~/BOSP$ ls -l
  total 20
  drwxr-xr-x 11 derkling derkling 4096 2011-07-20 16:52 barbeque
  drwxr-xr-x  4 derkling derkling 4096 2011-07-20 15:51 build
  drwxr-xr-x  3 derkling derkling 4096 2011-07-20 16:49 contrib
  drwxr-xr-x  5 derkling derkling 4096 2011-07-20 15:51 external
  -r--r--r--  1 derkling derkling   94 2011-07-20 15:51 Makefile

Where, the content of the available folders is:
- barbeque
	sources of the Barbeque RTRM framework
- build
	BOSP building system
- contrib
	third party contributed software
- external
	sources of dependency libraries
- out
	the target installation folder
- Makefile
	the main 

The installation process starts by compiling and installing all the external
components, than the Barbeque RTRM and finally all the contributed modules.
Each component is installed (by default) into the "out" subfolder, which thus,
at the end, will contains all the binary and libraries generated by the
building process.

The layout of the output folder is this:

  derkling@darkstar:out$ ls -la
  total 40
  drwxr-xr-x 10 derkling derkling 4096 2011-07-20 18:44 .
  drwxr-xr-x  8 derkling derkling 4096 2011-07-20 18:43 ..
  drwxr-xr-x  2 derkling derkling 4096 2011-07-20 18:44 bin
  drwxr-xr-x  3 derkling derkling 4096 2011-07-20 18:43 etc
  drwxr-xr-x  5 derkling derkling 4096 2011-07-20 18:44 include
  drwxr-xr-x  4 derkling derkling 4096 2011-07-20 18:44 lib
  drwxr-xr-x  2 derkling derkling 4096 2011-07-20 18:44 sbin
  drwxr-xr-x  3 derkling derkling 4096 2011-07-20 18:44 share
  drwxr-xr-x  5 derkling derkling 4096 2011-07-20 18:44 usr
  drwxr-xr-x  3 derkling derkling 4096 2011-07-20 18:43 var

Where, right now, these are the main interesting folders:
- etc/bbque/
	containing the Barbeque RTRM configuration files
- include/bbque/
	containing the header files required for application developement using
	the RTLib
- lib/bbque/
	containing the Barbeque RTLib and the framework provided plugins
- sbin/
	where the glorious Barbeque binary is deployed
- var/bbque/
	where run-time generated Barbeque files are places, e.g. the logfile of
	the cooking in progress


3. Running the Barbeque RTRM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once the Barbeque RTRM has been compiled, its binary is deployed under
<prefix>/sbin. This location has been choosed since this framework should be
run with root priviledges possibily at system boot time.

.:: Setup the configuration file

Before running Barbeque it is worth to have a look at its configuration file:
<prefix>/etc/bbque/bbque.conf. This file collects all the supported
configuration options grouped in sections which corresponds to the main
framework modules. All the options are by default commented ad defined as by
default. In case an option should be canged, before starting Barbeque, uncoment
it and set it to the proper value.

Among all the available configuration options, a user willing to understand the
Barbeque internals could be interested into making the logtrace more verbose.
In this case, under the [log4cpp] section, a set of "category.*" options are
provided which could be used to defined the minimum loglevel for each Barbeque
module (please refer to the Barbeque documentation for a complete description
of available modules).


.:: Run the resource manager

To run the Barbeque RTRM, simply do:

  $ <prefix>/sbin/barbeque

The default configuration redirects the complete logtrace to the
<prefix>/var/bbque/bbque.log. If you prefers to get the logtrace directly on
the console, you could simply enable the "raConsole" rootCategory option under
the [log4cpp] (by commenting out the default raFile options).

Please note that, for the time being, the Barbeque RTRM has not a "demonize"
option, thus it will not detach from the current console.


4. Running the provided Tutorials
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A good starting point, especially for application developers, is to have a look
at the tutorials provided by the contrib/rtlib_tutorials project. Once compiled
they are deployed under <prefix>/usr/bin. With a running instance of Barbeque,
move under this folder and run them in order to read about the RTLib,
understand how to use it and actually see its interaction with the resource
manager thanks to the provided inline logtrace.

